package com.sockerhalt;

import com.mongodb.*;
import com.sockerhalt.product.Product;
import com.sockerhalt.product.ProductAdaptor;
import com.sockerhalt.product.ProductBuilder;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.net.UnknownHostException;

public class Sockerhalt {
    private static final Logger LOG = LogManager.getLogger(Sockerhalt.class);

    private static final String MONGO_URI = "mongodb://localhost:27017";
    private static final String XML_PATH = "https://www.systembolaget.se/api/assortment/products/xml";

    static MongoClient MONGO_CLIENT;
    static DB DATABASE;
    static DBCollection PRODUCT_COLLECTION;
    public static HttpClient HTTP_CLIENT;

    public static void main(String[] args) {
        try {
            MONGO_CLIENT = new MongoClient(new MongoClientURI(MONGO_URI));
            DATABASE = MONGO_CLIENT.getDB("sockerhalt");
            PRODUCT_COLLECTION = DATABASE.getCollection("products");
        } catch (UnknownHostException e) {
            LOG.error("Failed to connect to {}, reason: {}", MONGO_URI, e.getMessage());
            System.exit(1);
        }

        int timeout = 5;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();

        HTTP_CLIENT = HttpClientBuilder.create().setDefaultRequestConfig(config).build();

        var xmlGet = new HttpGet(XML_PATH);

        String filePath = "products.xml";
        if (!new File(filePath).exists()) {
            try {
                var response = HTTP_CLIENT.execute(xmlGet);
                if (response.getStatusLine().getStatusCode() == 200) {
                    InputStream is = response.getEntity().getContent();
                    FileOutputStream fos = new FileOutputStream(new File(filePath));
                    int inByte;
                    while ((inByte = is.read()) != -1)
                        fos.write(inByte);
                    is.close();
                    fos.close();
                    EntityUtils.consumeQuietly(response.getEntity());
                } else {
                    throw new IOException(response.getStatusLine().toString());
                }
            } catch (IOException e) {
                LOG.error("Failed to download xml from {}, reason: {}", XML_PATH, e.getMessage());
                System.exit(1);
            } finally {
                xmlGet.releaseConnection();
            }
        }

        try {
            SAXReader reader = new SAXReader();
            Document document = reader.read(new File(filePath));
            treeWalk(document);
            LOG.info("Finished reading all products");
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public static void treeWalk(Document document) {
        treeWalk(document.getRootElement());
    }

    public static void treeWalk(Element element) {
        for (int i = 0, size = element.nodeCount(); i < size; i++) {
            Node node = element.node(i);
            if (node instanceof Element) {
                if (((Element) node).getQName().getName().equals("artikel")) {
                    DBObject query = new BasicDBObject("_id", Integer.valueOf(node.selectSingleNode("nr").getText()));
                    DBCursor cursor = PRODUCT_COLLECTION.find(query);
                    DBObject productObject = cursor.one();
                    if (productObject == null) {
                        ProductBuilder productBuilder = ProductBuilder.aProduct((Element) node);
                        if (productBuilder != null) {
                            Product product = productBuilder.build();
                            product.setSugarContent(product.fetchSugarContent());
                            PRODUCT_COLLECTION.insert(ProductAdaptor.toDBObject(product));
                        }
                    }
                } else {
                    treeWalk((Element) node);
                }
            } else {
                System.out.println(node);
                // do something…
            }
        }
    }

}