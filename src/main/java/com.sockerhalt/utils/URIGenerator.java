package com.sockerhalt.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.AbstractMap;
import java.util.Map;

public class URIGenerator {
    private static final String DRINKS_ROOT_PATH = "https://www.systembolaget.se/dryck/";
    private static final String SEARCH_ROOT_PATH =
            "https://www.systembolaget.se/api/productsearch/search/sok-dryck/?searchquery=PRODUCTID&sortdirection=Ascending&site=all&fullassortment=1";

    private static final Map<String, String> GROUP_MAPPINGS = Map.ofEntries(
            new AbstractMap.SimpleEntry<>("Vitt vin", "vita-viner"),
            new AbstractMap.SimpleEntry<>("Vita", "vita-viner"),
            new AbstractMap.SimpleEntry<>("Rött vin", "roda-viner"),
            new AbstractMap.SimpleEntry<>("Röda", "roda-viner"),
            new AbstractMap.SimpleEntry<>("Mousserande vin", "mousserande-viner"),
            new AbstractMap.SimpleEntry<>("Rosévin", "roseviner"),
            new AbstractMap.SimpleEntry<>("Rosé", "roseviner"),
            new AbstractMap.SimpleEntry<>("Okryddad sprit", "sprit"),
            new AbstractMap.SimpleEntry<>("Kryddad sprit", "sprit"),
            new AbstractMap.SimpleEntry<>("Whisky", "sprit"),
            new AbstractMap.SimpleEntry<>("Rom", "sprit"),
            new AbstractMap.SimpleEntry<>("Tequila och Mezcal", "sprit"),
            new AbstractMap.SimpleEntry<>("Cognac", "sprit"),
            new AbstractMap.SimpleEntry<>("Gin", "sprit"),
            new AbstractMap.SimpleEntry<>("Punsch", "sprit"),
            new AbstractMap.SimpleEntry<>("Likör", "sprit"),
            new AbstractMap.SimpleEntry<>("Brandy och Vinsprit", "sprit"),
            new AbstractMap.SimpleEntry<>("Bitter", "sprit"),
            new AbstractMap.SimpleEntry<>("Aniskryddad sprit", "sprit"),
            new AbstractMap.SimpleEntry<>("Smaksatt sprit", "sprit"),
            new AbstractMap.SimpleEntry<>("Grappa och Marc", "sprit"),
            new AbstractMap.SimpleEntry<>("Snaps", "sprit"),
            new AbstractMap.SimpleEntry<>("Drinkar och Cocktails", "sprit"),
            new AbstractMap.SimpleEntry<>("Armagnac", "sprit"),
            new AbstractMap.SimpleEntry<>("Genever", "sprit"),
            new AbstractMap.SimpleEntry<>("Sprit av frukt", "sprit"),
            new AbstractMap.SimpleEntry<>("Calvados", "sprit"),
            new AbstractMap.SimpleEntry<>("Cider", "cider-och-blanddrycker"),
            new AbstractMap.SimpleEntry<>("Blanddrycker", "cider-och-blanddrycker"),
            new AbstractMap.SimpleEntry<>("Apertif", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Mjöd", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Sake", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Montilla", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Sherry", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Juldrycker", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Blå mousserande", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Vermouth", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Fruktvin", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Glögg och Glühwein", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Smaksatt vin", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Portvin", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Madeira", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Vin av flera typer", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Blå stilla", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Övrigt starkvin", "apertif-dessert"),
            new AbstractMap.SimpleEntry<>("Öl", "ol"));

    public static String generateProductURI(int id, String name, String group) {
        return DRINKS_ROOT_PATH + GROUP_MAPPINGS.get(group) + "/"
                + StringUtils.lowerCase(name)
                .replace(" ", "-")
                .replace("&", "")
                .replace("%", "")
                .replace("´", "")
                .replace("`", "")
                .replace("\"", "")
                + "-" + id;
    }

    public static String generateSearchURI(int id) {
        return SEARCH_ROOT_PATH.replace("PRODUCTID", "" + id);
    }
}
