package com.sockerhalt.product;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class ProductAdaptor {
    public static final DBObject toDBObject(Product product) {
        return new BasicDBObject("_id", product.getId())
                .append("articleId", product.getArticleId())
                .append("productId", product.getProductId())
                .append("name", product.getName())
                .append("altName", product.getAltName())
                .append("price", product.getPrice())
                .append("volume", product.getVolume())
                .append("expired", product.isExpired())
                .append("group", product.getGroup())
                .append("packaging", product.getPackaging())
                .append("closure", product.getClosure())
                .append("origin", product.getOrigin())
                .append("originCountry", product.getOriginCountry())
                .append("producer", product.getProducer())
                .append("distributor", product.getDistributor())
                .append("year", product.getYear())
                .append("percentage", product.getPercentage())
                .append("selection", product.getSelection())
                .append("selectionText", product.getSelectionText())
                .append("eco", product.isEco())
                .append("ethical", product.isEthical())
                .append("ethicalLabel", product.getEthicalLabel())
                .append("kosher", product.isKosher())
                .append("contents", product.getContents())
                .append("sugarContent", product.getSugarContent())
                .append("sugarContentValue", product.getSugarContentValue());
    }
}
