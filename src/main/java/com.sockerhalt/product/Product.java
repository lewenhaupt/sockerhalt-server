package com.sockerhalt.product;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sockerhalt.Sockerhalt;
import com.sockerhalt.utils.URIGenerator;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class Product {
    private static final Logger LOG = LogManager.getLogger(Product.class);

    private int id; // nr
    private int articleId; // Artikelid
    private int productId; // Varnummer
    private String name; // Namn
    private String altName; //Namn2
    private Double price; // Prisinklmoms
    private Double volume; // Volym
    private Boolean expired; // Utgátt
    private String group; // Varugrupp
    private String type; // Typ
    private String packaging; // Forpackning
    private String closure; // Forslutning
    private String origin; // Ursprung
    private String originCountry; // Ursprunglandnamn
    private String producer; // Producent
    private String distributor; // Leverantor
    private Integer year; // Argang
    private Double percentage; // Alkoholhalt
    private String selection; // Sortiment
    private String selectionText; // SortimentText
    private Boolean eco; // Ekologisk
    private Boolean ethical; // Etiskt
    private String ethicalLabel; // EtisktEtikett
    private Boolean kosher;
    private String contents; // RavarorBeskrivning
    private String sugarContent;
    private Double sugarContentValue;

    Product() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAltName() {
        return altName;
    }

    public void setAltName(String altName) {
        this.altName = altName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Boolean isExpired() {
        return expired;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPackaging() {
        return packaging;
    }

    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }

    public String getClosure() {
        return closure;
    }

    public void setClosure(String closure) {
        this.closure = closure;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getDistributor() {
        return distributor;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public String getSelectionText() {
        return selectionText;
    }

    public void setSelectionText(String selectionText) {
        this.selectionText = selectionText;
    }

    public Boolean isEco() {
        return eco;
    }

    public void setEco(Boolean eco) {
        this.eco = eco;
    }

    public Boolean isEthical() {
        return ethical;
    }

    public void setEthical(Boolean ethical) {
        this.ethical = ethical;
    }

    public String getEthicalLabel() {
        return ethicalLabel;
    }

    public void setEthicalLabel(String ethicalLabel) {
        this.ethicalLabel = ethicalLabel;
    }

    public Boolean isKosher() {
        return kosher;
    }

    public void setKosher(Boolean kosher) {
        this.kosher = kosher;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getSugarContent() {
        return sugarContent;
    }

    public void setSugarContent(String sugarContent) {
        this.sugarContent = sugarContent != null ? sugarContent : "N/A";
        this.sugarContentValue = sugarContent != null ? Double.valueOf(sugarContent.replace("<", "")
                .replace(" g/l", "")
                .replace(",", ".")) : null;
    }

    public Double getSugarContentValue() {
        return sugarContentValue;
    }

    public String fetchSugarContent() {
//        "https://www.systembolaget.se/api/productsearch/search/sok-dryck/?searchquery=7767101&sortdirection=Ascending&site=all&fullassortment=1"
        String URI = URIGenerator.generateProductURI(getId(), getName(), getGroup());
        if (LOG.isDebugEnabled()) {
            LOG.debug("Fetching sugar content for {} using URI: {}", getName(), URI);
        }
        var getProduct = new HttpGet(URI);
        try {
            var response = Sockerhalt.HTTP_CLIENT.execute(getProduct);
            if (response.getStatusLine().getStatusCode() == 200) {
                InputStream is = response.getEntity().getContent();
                org.jsoup.nodes.Document document = Jsoup.parse(is, null, URI, Parser.htmlParser());
                is.close();
                Elements lis = document.select(":matchesOwn(.*(g/l))");
                EntityUtils.consumeQuietly(response.getEntity());
                if (lis.size() > 0) {
                    Node textNode = lis.get(0).childNodes().get(0);
                    if (textNode instanceof TextNode) {
                        if (LOG.isInfoEnabled()) {
                            LOG.info("Sugar content found for {}", getName());
                        }
                        return ((TextNode) textNode).text();
                    } else {
                        LOG.error("Failed to get sugar content the standard way for {}, reason: {}", getName(), "Unknown");
                    }
                } else {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("No sugar content found for {}", getName());
                    }
                    return null;
                }
            } else {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Failed to get sugar content the standard way for {}, reason: {}", getName(), response.getStatusLine());
                }
                getProduct.releaseConnection();
                String searchURI = URIGenerator.generateSearchURI(getId());
                var searchRequest = new HttpGet(searchURI);
                try {
                    var searchResponse = Sockerhalt.HTTP_CLIENT.execute(searchRequest);
                    if (searchResponse.getStatusLine().getStatusCode() == 200) {
                        InputStream is = searchResponse.getEntity().getContent();
                        JsonParser parser = new JsonParser();
                        Reader reader = new InputStreamReader(is, "UTF-8");
                        JsonElement jsonElement = parser.parse(reader);
                        if (jsonElement.isJsonObject()) {
                            JsonObject jsonObject = jsonElement.getAsJsonObject();
                            if (jsonObject.has("ProductSearchResults")) {
                                JsonElement productSearchResultElement = jsonObject.get("ProductSearchResults");
                                if (productSearchResultElement.isJsonArray()) {
                                    JsonArray jsonArray = productSearchResultElement.getAsJsonArray();
                                    for (int i = 0; i < jsonArray.size(); i++) {
                                        JsonElement product = jsonArray.get(i);
                                        if (product.isJsonObject()) {
                                            JsonObject productObject = product.getAsJsonObject();
                                            if (productObject.has("ProductNumber")) {
                                                if (productObject.get("ProductNumber").getAsInt() == getId()) {
                                                    if (productObject.has("ProductUrl")) {
                                                        String productURL =
                                                                productObject.get("ProductUrl").getAsString();
                                                        String secondURI =
                                                                "https://www.systembolaget.se" + productURL;
                                                        if (LOG.isDebugEnabled()) {
                                                            LOG.debug("Fetching sugar content for {} using URI: {}", getName(), secondURI);
                                                        }
                                                        var getProductSecond = new HttpGet(secondURI);
                                                        try {
                                                            var responseSecond =
                                                                    Sockerhalt.HTTP_CLIENT.execute(getProductSecond);
                                                            if (responseSecond.getStatusLine().getStatusCode() == 200) {
                                                                InputStream is2 = response.getEntity().getContent();
                                                                org.jsoup.nodes.Document document2 =
                                                                        Jsoup.parse(is2, null, URI, Parser.htmlParser());
                                                                is2.close();
                                                                Elements lis2 =
                                                                        document2.select(":matchesOwn(.*(g/l))");
                                                                EntityUtils.consumeQuietly(responseSecond.getEntity());
                                                                if (lis2.size() > 0) {
                                                                    Node textNode = lis2.get(0).childNodes().get(0);
                                                                    if (textNode instanceof TextNode) {
                                                                        if (LOG.isInfoEnabled()) {
                                                                            LOG.info("Sugar content found for {} during alternate method", getName());
                                                                        }
                                                                        return ((TextNode) textNode).text();
                                                                    } else {
                                                                        LOG.error("Failed to get sugar content the standard way for {}, reason: {}", getName(), "Unknown");
                                                                    }
                                                                } else {
                                                                    if (LOG.isDebugEnabled()) {
                                                                        LOG.debug("No sugar content found for {}", getName());
                                                                    }
                                                                    return null;
                                                                }
                                                            } else {
                                                                if (LOG.isDebugEnabled()) {
                                                                    LOG.debug("No sugar content found for {} during alternate method", getName());
                                                                }
                                                                return null;
                                                            }
                                                        } catch (IOException e3) {
                                                            LOG.error("Failed to get sugar content for {}, reason: {}", getName(), e3
                                                                    .getMessage());
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // TODO: Change to read the JSON output from the api
                        }
                        is.close();
                    }
                } catch (IOException e2) {
                    LOG.error("Failed to get sugar content for {}, reason: {}", getName(), e2.getMessage());
                } finally {
                    searchRequest.releaseConnection();
                }

                // TODO: Try a different way where we search for the id instead and from there get the link
            }
        } catch (
                IOException e) {
            LOG.error("Failed to get sugar content for {}, reason: {}", getName(), e.getMessage());
        } finally {
            getProduct.releaseConnection();
        }
        return null;
    }

}
