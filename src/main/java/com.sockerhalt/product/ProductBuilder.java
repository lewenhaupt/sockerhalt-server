package com.sockerhalt.product;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.Element;

public final class ProductBuilder {
    private static final Logger LOG = LogManager.getLogger(ProductBuilder.class);

    private int id; // nr
    private int articleId; // Artikelid
    private int productId; // Varnummer
    private String name; // Namn
    private String altName; //Namn2
    private Double price; // Prisinklmoms
    private Double volume; // Volym
    private Boolean expired; // Utgátt
    private String group; // Varugrupp
    private String type; // Typ
    private String packaging; // Forpackning
    private String closure; // Forslutning
    private String origin; // Ursprung
    private String originCountry; // Ursprunglandnamn
    private String producer; // Producent
    private String distributor; // Leverantor
    private Integer year; // Argang
    private Double percentage; // Alkoholhalt
    private String selection; // Sortiment
    private String selectionText; // SortimentText
    private Boolean eco; // Ekologisk
    private Boolean ethical; // Etiskt
    private String ethicalLabel; // EtisktEtikett
    private Boolean kosher; // Koscher
    private String contents; // RavarorBeskrivning

    private ProductBuilder() {
    }

    public static ProductBuilder aProduct(Element element) {
        ProductBuilder productBuilder = new ProductBuilder();
        try {
            productBuilder.withId(Integer.valueOf(element.selectSingleNode("nr").getText()));
            productBuilder.withArticleId(Integer.valueOf(element.selectSingleNode("Artikelid").getText()));
            productBuilder.withProductId(Integer.valueOf(element.selectSingleNode("Varnummer").getText()));
        } catch (NullPointerException | NumberFormatException e) {
            LOG.error("Failed to create product, one or more required parameters are missing, reason: {}", e.getMessage());
            return null;
        }

        productBuilder.withName(getOptionalString(element, "Namn"));
        productBuilder.withAltName(getOptionalString(element, "Namn2"));
        productBuilder.withPrice(getOptionalDouble(element, "Prisinklmoms"));
        productBuilder.withVolume(getOptionalDouble(element, "Volym"));
        productBuilder.withExpired(getOptionalBoolean(element, "Utgått"));
        productBuilder.withGroup(getOptionalString(element, "Varugrupp"));
        productBuilder.withType(getOptionalString(element, "Typ"));
        productBuilder.withPackaging(getOptionalString(element, "Forpackning"));
        productBuilder.withClosure(getOptionalString(element, "Forslutning"));
        productBuilder.withOrigin(getOptionalString(element, "Ursprung"));
        productBuilder.withOriginCountry(getOptionalString(element, "Ursprunglandnamn"));
        productBuilder.withProducer(getOptionalString(element, "Producent"));
        productBuilder.withDistributor(getOptionalString(element, "Leverantor"));
        productBuilder.withYear(getOptionalInteger(element, "Argang"));
        productBuilder.withPercentage(getOptionalDouble(element, "Alkoholhalt"));
        productBuilder.withSelection(getOptionalString(element, "Sortiment"));
        productBuilder.withSelectionText(getOptionalString(element, "SortimentText"));
        productBuilder.withEco(getOptionalBoolean(element, "Ekologisk"));
        productBuilder.withEthical(getOptionalBoolean(element, "Etiskt"));
        productBuilder.withEthicalLabel(getOptionalString(element, "EtisktEtikett"));
        productBuilder.withKosher(getOptionalBoolean(element, "Koscher"));
        productBuilder.withContents(getOptionalString(element, "RavarorBeskrivning"));

        return productBuilder;
    }

    private static Integer getOptionalInteger(Element element, String name) {
        return element.selectSingleNode(name) != null && !element.selectSingleNode(name).getText().isEmpty()
                ? Integer.valueOf(element.selectSingleNode(name).getText()) : null;
    }

    private static boolean getOptionalBoolean(Element element, String name) {
        return element.selectSingleNode(name) != null && !element.selectSingleNode(name).getText().isEmpty()
                ? Boolean.valueOf(element.selectSingleNode(name).getText()) : false;
    }

    private static Double getOptionalDouble(Element element, String name) {
        return element.selectSingleNode(name) != null && !element.selectSingleNode(name).getText().isEmpty()
                ? Double.valueOf(element.selectSingleNode(name).getText().replace("%", "")) : null;
    }

    private static String getOptionalString(Element element, String name) {
        return element.selectSingleNode(name) != null && !element.selectSingleNode(name).getText().isEmpty()
                ? element.selectSingleNode(name).getText() : null;
    }

    public static ProductBuilder aProduct(int id, int articleId, int productId) {
        return new ProductBuilder()
                .withId(id)
                .withArticleId(articleId)
                .withProductId(productId);
    }

    private ProductBuilder withId(int nr) {
        this.id = nr;
        return this;
    }

    private ProductBuilder withArticleId(int articleId) {
        this.articleId = articleId;
        return this;
    }

    private ProductBuilder withProductId(int productId) {
        this.productId = productId;
        return this;
    }

    public ProductBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public ProductBuilder withAltName(String altName) {
        this.altName = altName;
        return this;
    }

    public ProductBuilder withPrice(Double price) {
        this.price = price;
        return this;
    }

    public ProductBuilder withVolume(Double volume) {
        this.volume = volume;
        return this;
    }

    public ProductBuilder withExpired(boolean expired) {
        this.expired = expired;
        return this;
    }

    public ProductBuilder withGroup(String group) {
        this.group = group;
        return this;
    }

    public ProductBuilder withType(String type) {
        this.type = type;
        return this;
    }

    public ProductBuilder withPackaging(String packaging) {
        this.packaging = packaging;
        return this;
    }

    public ProductBuilder withClosure(String closure) {
        this.closure = closure;
        return this;
    }

    public ProductBuilder withOrigin(String origin) {
        this.origin = origin;
        return this;
    }

    public ProductBuilder withOriginCountry(String originCountry) {
        this.originCountry = originCountry;
        return this;
    }

    public ProductBuilder withProducer(String producer) {
        this.producer = producer;
        return this;
    }

    public ProductBuilder withDistributor(String distributor) {
        this.distributor = distributor;
        return this;
    }

    public ProductBuilder withYear(Integer year) {
        this.year = year;
        return this;
    }

    public ProductBuilder withPercentage(Double percentage) {
        this.percentage = percentage;
        return this;
    }

    public ProductBuilder withSelection(String selection) {
        this.selection = selection;
        return this;
    }

    public ProductBuilder withSelectionText(String selectionText) {
        this.selectionText = selectionText;
        return this;
    }

    public ProductBuilder withEco(boolean eco) {
        this.eco = eco;
        return this;
    }

    public ProductBuilder withEthical(boolean ethical) {
        this.ethical = ethical;
        return this;
    }

    public ProductBuilder withEthicalLabel(String ethicalLabel) {
        this.ethicalLabel = ethicalLabel;
        return this;
    }

    public ProductBuilder withKosher(boolean kosher) {
        this.kosher = kosher;
        return this;
    }

    public ProductBuilder withContents(String contents) {
        this.contents = contents;
        return this;
    }

    public Product build() {
        Product product = new Product();
        product.setId(id);
        product.setArticleId(articleId);
        product.setProductId(productId);
        product.setName(name);
        product.setAltName(altName);
        product.setPrice(price);
        product.setVolume(volume);
        product.setExpired(expired);
        product.setGroup(group);
        product.setType(type);
        product.setPackaging(packaging);
        product.setClosure(closure);
        product.setOrigin(origin);
        product.setOriginCountry(originCountry);
        product.setProducer(producer);
        product.setDistributor(distributor);
        product.setYear(year);
        product.setPercentage(percentage);
        product.setSelection(selection);
        product.setSelectionText(selectionText);
        product.setEco(eco);
        product.setEthical(ethical);
        product.setEthicalLabel(ethicalLabel);
        product.setKosher(kosher);
        product.setContents(contents);
        return product;
    }
}
